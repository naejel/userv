try:
    import usocket as socket
except:
    import socket

try:
    import urequests as requests
except:
    import request


class uServ:
    def __init__(self, port=80, max_conn=5, buffer_size=1024):
        self.routes = []
        self.files = []
        self.conn = None
        self.addr = None
        self.port = port
        self.headers = []
        self.footers = []
        self.error_404 = "404"
        self.buffer_size = buffer_size
        self.max_conn = max_conn
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind(('', self.port))
        self.sock.listen(self.max_conn)

    def set404(self, error_message):
        self.error_404 = error_message

    def add_header(self, header):
        self.headers.append(header)

    def add_footer(self, footer):
        self.footers.append(footer)

    def add_route(self, route):
        self.routes.append(route)

    def add_file(self, file):
        self.files.append(file)

    def get_route(self, method, route, params):
        found = False
        for _route in self.routes:
            if _route[0] == method and _route[1] == route:
                found = True
                self.send(_route[2](params))
        for _file in self.files:
            if _file == route:
                found = True
                with open(_file, "r") as f:
                    self.conn.send(f.read())
                    self.conn.close()
        if not found:
            self.send(self.error_404)

    def parse_request(self, recv):
        req = recv.decode('UTF-8').split(':')[0]
        method = req.split()[0]
        route = req.split()[1]
        params = []
        for prms in route.split('?'):
            params.append(prms)
        route = params[0]
        params.pop(0)
        self.get_route(method, route, params)

    def send(self, to_send):
        ts = "<head>\n"
        for _header in self.headers:
            ts += _header + "\n"
        ts += "\n</head>\n<body>\n"
        ts += to_send
        ts += "\n</body>"
        ts += "<footer>\n"
        for _footer in self.footers:
            ts += _footer + "\n"
        ts += "\n</footer>"
        self.conn.send(ts)
        self.conn.close()

    def loop(self):
        self.conn, self.addr = self.sock.accept()
        self.parse_request(self.conn.recv(self.buffer_size))
