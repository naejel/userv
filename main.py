from userv import uServ
import gc
import network

gc.collect()

ap = network.WLAN(network.AP_IF)
ap.active(True)
ap.config(essid="uServTest")

server = uServ()


def test(params):
    to_return = "<h1>TEST</h1>\n"
    for prms in params:
        to_return += str(prms) + "<br>\n"
    return to_return


def index(params):
    return "<h1>TEST IMG</h1>\n<img src =\"img.jpg\">"


server.add_route(("GET", "/", index))
server.add_route(("GET", "/test", test))

server.add_file("/style.css")
server.add_file("/img.jpg")
server.add_file("/favicon.ico")

server.add_header("<title>uServ</title>")
server.add_header(
    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">")
server.add_header("<link rel=\"stylesheet\" href=\"/style.css\">")

server.add_footer("<p>By NaejEl</p>")

server.set404("Nothing Here")

while True:
    server.loop()
